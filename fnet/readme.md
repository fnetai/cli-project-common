**Important Note:** This document will automatically update, so please do not make any changes. Otherwise, you might lose them.

# Flow Node Project Setup Guide

## Prerequisites

1. Ensure you have `@fnet/cli` installed globally. If not, install it using:
```bash
npm i @fnet/cli -g
```

2. Once installed, two binary commands will be available: `fnet` and `fnode`. Use `fnode` for flow node projects.

## Identifying Project Type

- If there's a `node.yaml` in the project directory, it's a flow node project.
- Use `fnode` commands for operations related to flow node projects.

## Building the Project

To compile the project, use:
```bash
fnode build
```
This will generate a `.workspace` directory containing all necessary files and configurations for debugging, building, and deploying the project.

## Watching the Project

To run the project in development mode, use:
```bash
fnode watch
```